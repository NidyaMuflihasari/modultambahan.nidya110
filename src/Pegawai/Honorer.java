package Pegawai;

import java.util.Scanner;

public class Honorer extends Pegawai {

    public Honorer() {
    }

    @Override
    public double hitungGatot() {
        return gajiPokok;
    }

    public void inputDataHonorer() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama     : ");
        nama = sc.next();
        System.out.print("Nip      : ");
        nip = sc.next();
        System.out.print("Golongan : ");
        golongan = sc.nextInt();
    }
}
