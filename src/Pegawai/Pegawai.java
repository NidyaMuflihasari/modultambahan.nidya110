package Pegawai;

public class Pegawai implements Pendapatan {

    protected String nip, nama;
    protected double gajiTotal, gajiPokok;
    protected int golongan;

    public Pegawai() {
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getGajiTotal() {
        return gajiTotal;
    }

    public void setGajiTotal(double gajiTotal) {
        this.gajiTotal = gajiTotal;
    }

    public int getGolongan() {
        return golongan;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public double hitGajiPokok() {
        switch (golongan) {
            case 1:
                gajiPokok = 500000;
                break;
            case 2:
                gajiPokok = 750000;
                break;
            case 3:
                gajiPokok = 1000000;
                break;
            default:
                break;
        }
        return gajiPokok;
    }

    @Override
    public double hitungGatot() {
        return gajiPokok;
    }
}
