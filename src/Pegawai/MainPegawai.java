package Pegawai;

import java.util.Scanner;

public class MainPegawai {

    static Scanner sc = new Scanner(System.in);
    static Pegawai[] pgw;
    static Kantor kantor = new Kantor();
    static Manager mng = new Manager();
    static int banyakPgw, pilih;
    static double totalGajiPgw;

    public static void main(String[] args) {
        System.out.println("Masukan Data Manager  ");
        mng.inputDataManager();
        kantor.setManager(mng);
        do {
            System.out.println("Masukan Banyak Pegawai");
            banyakPgw = sc.nextInt();
        } while (banyakPgw <= 0);
        System.out.println("Masukan Data Pegawai");
        System.out.println("Jenis Pegawai");
        System.out.println("1. Marketing");
        System.out.println("2. Honorer");
        System.out.println();
        pgw = new Pegawai[banyakPgw];
        for (int i = 0; i < banyakPgw; i++) {
            System.out.println("Masukan Data Anggota ke-" + (i + 1));
            do {
                System.out.print("Pilih Jenis Pegawai : ");
                pilih = sc.nextInt();
            } while (pilih <= 0 && pilih > 2);
            if (pilih == 1) {
                System.out.println("Marketing");
                Marketing mrkt = new Marketing();
                mrkt.inputDataMarketing();
                pgw[i] = mrkt;
            } else if (pilih == 2) {
                System.out.println("Honorer");
                Honorer hnr = new Honorer();
                hnr.inputDataHonorer();
                pgw[i] = hnr;
            }
            System.out.println();
        }
        kantor.setPegawai(pgw);
        System.out.println("Nama Manager " + mng.getNama());
        System.out.println("Data Pegawai");
        System.out.print("============================================================");
        System.out.println("==========================================================");
        System.out.println("No.\t Nama\t\t Status\t\t Gaji Pokok\t Golongan\t Tunjangan\t Lembur\t\t "
                + "Gaji Total");
        System.out.print("============================================================");
        System.out.println("==========================================================");
        for (int i = 0; i < banyakPgw; i++) {
            System.out.print((i + 1) + "\t");
            if (pgw[i] instanceof Marketing) {
                Marketing mrkt = (Marketing) pgw[i];
                System.out.print(mrkt.getNama() + "\t\t");
                if (mrkt.isSelesai() == true) {
                    System.out.print("Selesai\t\t");
                } else {
                    System.out.print("Studi\t\t");
                }
                System.out.print(mrkt.hitGajiPokok() + "\t");
                System.out.print(mrkt.getGolongan() + "\t\t");
                System.out.print("-\t\t\t");
                System.out.print(mrkt.hitLembur() + "\t\t");
                System.out.println(mrkt.hitungGatot());
            }
            if (pgw[i] instanceof Honorer) {
                Honorer hnr = (Honorer) pgw[i];
                System.out.print(hnr.getNama() + "\t\t");
                System.out.print("-\t\t");
                System.out.print(hnr.hitGajiPokok() + "\t\t");
                System.out.print(hnr.getGolongan());
                System.out.print("-\t\t");
                System.out.print("-\t\t");
                System.out.println(hnr.hitungGatot());
            }
            System.out.print("============================================================");
            System.out.println("==========================================================");
            System.out.println();
        }
        System.out.println("Jumlah Gaji Pegawai : " + kantor.hitGajiPeg());
    }
}
