package Pegawai;

public class Kantor {

    private Manager manager;
    private Pegawai[] pegawai;

    public Kantor() {
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public double hitGajiPeg() {
        double totalSeluruhGaji = 0;
        for (int i = 0; i < pegawai.length; i++) {
            totalSeluruhGaji += pegawai[i].hitungGatot();
        }
        totalSeluruhGaji += manager.hitungGatot();
        return totalSeluruhGaji;
    }
}
