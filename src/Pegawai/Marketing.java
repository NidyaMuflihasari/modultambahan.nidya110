package Pegawai;

import java.util.Scanner;

public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;
    private double lembur;
    private boolean studi;

    public Marketing() {
    }
   
    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public double hitLembur() {
        if (jamKerja > 8) {
            lembur = (jamKerja - 8) * 50000;
        } else {
            lembur = 0;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        return gajiPokok + lembur;
    }

    public void inputDataMarketing() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama        : ");
        nama = sc.next();
        System.out.print("Nip         : ");
        nip = sc.next();
        System.out.print("Golongan    : ");
        golongan = sc.nextInt();
        System.out.print("Jam Kerja   : ");
        jamKerja = sc.nextInt();
        System.out.print("Lama Lembur : ");
        lembur = sc.nextDouble();
    }
    
    @Override
    public boolean isSelesai() {
        return studi;
    }
    
    @Override
    public void setStudi(boolean studi){
        this.studi= studi;
    }
}
