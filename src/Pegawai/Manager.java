package Pegawai;

import java.util.Scanner;

public class Manager extends Pegawai implements TugasBelajar {

    private int jumAnak, jamKerja, istri;
    private double tunjangan, lembur;
    private boolean studi;

    public Manager() {
    }

    public int getJumAnak() {
        return jumAnak;
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getIstri() {
        return istri;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public double hitTunjangan() {
        if (jumAnak > 0 && jumAnak <= 3) {
            tunjangan = jumAnak * 100000;
        } else {
            tunjangan = 0;
        }
        return tunjangan;
    }

    public double hitLembur() {
        if (jamKerja > 8) {
            lembur = (jamKerja - 8) * 50000;
        } else {
            lembur = 0;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        return gajiPokok + tunjangan + lembur;
    }

    public void inputDataManager() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama         : ");
        nama = sc.next();
        System.out.print("Nip          : ");
        nip = sc.next();
        System.out.print("Golongan     : ");
        golongan = sc.nextInt();
        System.out.print("Jumlah Anak  : ");
        jumAnak = sc.nextInt();
        System.out.print("Jumlah Istri : ");
        istri = sc.nextInt();
    }

    @Override
    public boolean isSelesai() {
        return studi;
    }

    @Override
    public void setStudi(boolean studi) {
        this.studi = studi;
    }
}
